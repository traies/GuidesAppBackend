import express from "express";
import dotenv from "dotenv";
import { Sequelize } from "sequelize-typescript";
import * as GuideController from "./controllers/GuideController";
import * as UserController from "./controllers/UserController";
import User from "./models/User";
import "./persistence/DB";
import Lead from "./models/Lead";
import Person from "./models/Person";
import Trip from "./models/Trip";
import Guide from "./models/Guide";

dotenv.config({ path: ".env"});

const app = express();

// Express config
app.set("port", process.env.PORT || 3000);

/**
 * Routes
 */
app.get("/guide/:id", GuideController.getGuide);
app.get("/user/:id", UserController.getUser);
app.get("/lead/:id", (req: express.Request, res: express.Response) => {
	const id = +req.param("id");
	Guide.findById(id,{
		attributes: ["id"],
		include: [ {
			as: "leads",
			model: Lead,
			include: [Trip, Person]
		}]
	}).then((data) => {
		res.json(data);
	});
});

app.listen(app.get("port"), () => {
	console.log("Press CTRL-C to stop.\n");
});