import * as GuidesPersistence from "../persistence/GuidesPersistence";

export function getGuide(id: number): Promise<any> {
	return GuidesPersistence.getGuideData(id).catch((err: any) => {
		console.log();
		return {};
	});
}