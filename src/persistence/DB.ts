import {Sequelize} from "sequelize-typescript";
import User from "../models/User";
import path from "path";
import Guide from "../models/Guide";
import Lead from "../models/Lead";
import Person from "../models/Person";
import Trip from "../models/Trip";

const sequelize = new Sequelize(
	{
		database: process.env.PGDATABASE,
		username: process.env.PGUSER,
		password: process.env.PGPASSWORD,
		host: process.env.PGHOST,
		dialect: 'postgres' ,
		operatorsAliases: false,
		pool: {
			max: 5,
			min: 0,
			acquire: 30000,
			idle: 10000
		},
	}
);

sequelize.addModels([User, Guide, Lead, Person, Trip]);