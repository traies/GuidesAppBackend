import { Table, Column, Model, DataType } from "sequelize-typescript";

@Table({
	tableName: "users",
})
export default class User extends Model<User> {
	
	@Column({
		primaryKey: true,
		type: DataType.INTEGER,
		autoIncrement: true,	
	})
	id: number;

	@Column({
		type: DataType.STRING
	})
	name: string;

	@Column({
		type: DataType.STRING
	})
	email: string;

	@Column({
		field: "is_admin",
		type: DataType.BOOLEAN,
	})
	isAdmin: boolean;
	
	@Column({
		field: "remember_token",
		type: DataType.STRING(100)
	})
	rememberToken: string;

	@Column({
		field: "created_at",
		type: DataType.DATE
	})
	createdAt: Date;

	@Column({
		field: "updated_at",
		type: DataType.DATE
	})
	updatedAt: Date;
}