import { Model, Table, PrimaryKey, Column, DataType } from "sequelize-typescript";

@Table({
	tableName: "trips"
})
export default class Trip extends Model<Trip> {

	@PrimaryKey
	@Column({
		allowNull: false,
	})
	id: number;

	@Column
	title: string;

	@Column({
		type: DataType.NUMERIC(10, 2),
	})
	price: number;
}