import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";
import Lead from "./Lead";

@Table({
	tableName: "guides",
})
export default class Guide extends Model<Guide> {
	@HasMany(() => Lead, "guide_id")
	leads: Lead[];

	@Column({
		type: DataType.INTEGER,
		primaryKey: true,
		allowNull: false,
	})
	id: number;

	@Column({
		type: DataType.STRING,
		allowNull: false,
		field: "first_name"
	})
	firstName: string;

	@Column({
		type: DataType.STRING,
		allowNull: false,
		field: "last_name",
	})
	lastName: string;

	@Column({
		type: DataType.STRING,
		allowNull: false,
	})
	email: string;

	@Column({
		type: DataType.INTEGER,
		allowNull: false,
		field: "cms_id"	
	})
	cmsId: number;

	@Column({
		field: "created_at",
		type: DataType.DATE
	})
	createdAt: Date;

	@Column({
		field: "updated_at",
		type: DataType.DATE
	})
	updatedAt: Date;

	@Column({
		type: DataType.STRING,
		allowNull: false,
		field: "legal_name",
	})
	legalName: string;

	@Column({
		type: DataType.STRING,
		field: "vat_number",
	})
	vatNumber: string;

	@Column({
		type: DataType.BOOLEAN,
		field: "eu_vat_validated",
		allowNull: false,
	})
	euVatValidated: boolean;

	@Column({
		type: DataType.STRING,
		field: "invoicing_category",
	})
	invoicingCategory: string;

	@Column({
		type: DataType.STRING
	})
	address: string;

	@Column({
		type: DataType.STRING,
		field: "address_number",
	})
	addressNumber: string;

	@Column({
		type: DataType.STRING,
		field: "post_zip_code",
	})
	postZipCode: string;
	
	@Column({
		type: DataType.STRING
	})
	city: string;
	
	@Column({
		type: DataType.STRING
	})
	country: string;

	@Column({
		type: DataType.STRING,
		field: "phone_number",
	})
	phoneNumber: string;

	@Column({
		type: DataType.STRING
	})
	box: string;

	@Column({
		type: DataType.STRING,
		field: "billing_email",
	})
	billingEmail: string;

	@Column({
		type: DataType.STRING,
		field: "billing_phone",
	})
	billingPhone: string;

	@Column({
		type: DataType.TEXT
	})
	comments: string;

	@Column({
		type: DataType.STRING,
		field: "billing_first_name",
	})
	billingFirstName: string;

	@Column({
		type: DataType.STRING,
		field: "billing_last_name",
	})
	billingLastName: string;

	@Column({
		type: DataType.STRING,
		field: "guide_secret",
	})
	guideSecret: string;

	@Column({
		type: DataType.STRING
	})
	hash: string;

	@Column({
		type: DataType.STRING
	})
	link: string;
	
	@Column({
		type: DataType.STRING,
		field: "local_name",
	})
	localName: string;

	// TODO: Add the rest of the guide attributes
}