import { Model, Table, Column, DataType } from "sequelize-typescript";

@Table({
	tableName: "persons"
})
export default class Person extends Model<Person> {

	@Column({
		type: DataType.INTEGER,
		primaryKey: true,
		allowNull: false,
	})
	id: number;

	@Column({
		type: DataType.STRING,
		allowNull: false,
	})
	email: string;

	@Column({
		type: DataType.STRING,
		allowNull: false,
		field: "first_name"
	})
	firstName: string;

	@Column({
		type: DataType.STRING,
		allowNull: false,
		field: "last_name",
	})
	lastName: string;

	@Column({
		type: DataType.STRING,
		allowNull: false,
		field: "fullname",
	})
	fullname: string;

	@Column({
		type: DataType.STRING(512),
		allowNull: false,
	})
	experience: string;

	@Column({
		field: "created_at",
		type: DataType.DATE
	})
	createdAt: Date;

	@Column({
		field: "updated_at",
		type: DataType.DATE
	})
	updatedAt: Date;
}