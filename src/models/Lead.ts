import { Model, Table, PrimaryKey, Column, DataType, BelongsTo } from "sequelize-typescript";
import Person from "./Person";
import Trip from "./Trip";
import Guide from "./Guide";

@Table({
	tableName: "leads",
})
export default class Lead extends Model<Lead> {
	@PrimaryKey
	@Column({
		allowNull: false
	})
	id: number;

	@BelongsTo(() => Person, "persons_id")
	person: Person;

	@BelongsTo(() => Trip, "trip_id")
	trip: Trip;

	@BelongsTo(() => Guide, "guide_id")
	guide: Guide;
}