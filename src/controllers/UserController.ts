import { Request, Response } from "express";
import User from "../models/User";
/**
 * Get /
 */
export function getUser(req: Request, res: Response): void {
	const id = +req.param("id");
	if (Number.isNaN(id) || !Number.isInteger(id) || id < 0) {
		res.status(400);
		res.send("Bad Request");
	} else {
		User.findById(id).catch((err) => {
			res.status(404);
			res.send("Not Found.");
		}).then((data) => {
			res.json(data);
		});
	}
}