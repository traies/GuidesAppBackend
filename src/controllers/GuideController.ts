import { Request, Response } from "express";
import * as GuideService from "../services/GuideService";
import Guide from "../models/Guide";
/**
 * Get /
 */
export function getGuide(req: Request, res: Response): void {
	const id = +req.param("id");
	if (Number.isNaN(id) || !Number.isInteger(id) || id < 0) {
		res.status(400);
		res.send("Bad Request");
	} else {
		Guide.findById(id).catch((err) => {
			res.status(404);
			res.send("Not Found.");
		}).then((data) => {
			res.json(data);
		});
	}
}